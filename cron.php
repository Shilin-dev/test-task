<?php
require('application/model/Table.php');
require('application/model/table/Table_Model_Messages.php');
require('application/config/Database.php');
include_once('application/controllers/Controller_Cron.php');

use application\controllers\Controller_Cron;

$cron = new Controller_Cron();
$cron->sendMessages();