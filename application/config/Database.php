<?php
namespace application\config;

use \PDO;

class Database
{
    static $_connection;

    public static function getConnection()
    {
        if (!self::$_connection) {
            $dsn = 'mysql:host=localhost;port=3306;dbname=test-task';
            $username = 'batkers';
            $password = '123098';
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8 COLLATE utf8_general_ci',);
            self::$_connection = @new PDO($dsn, $username, $password, $options);
            self::$_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$_connection;
    }
}