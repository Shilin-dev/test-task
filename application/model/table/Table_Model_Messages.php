<?php
namespace application\model\table;

use \application\model\Model_Table;
use \PDO;

class Table_Model_Messages extends Model_Table
{
    public $id;
    public $name;
    public $message;
    public $date_created;
    public $sent;

    public function getTableName()
    {
        return 'Messages';
    }

    public function fetchSentMessages()
    {
        $tpl = 'SELECT * FROM %s WHERE `sent` IS NOT true AND `date_created` < DATE_SUB(NOW(),INTERVAL 10 MINUTE)';
        $sql = sprintf($tpl, $this->getTableName());
        $va = $this->db()->query($sql);
        $result = $va->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function changeSentStatusById($id)
    {
        $tpl = "UPDATE %s SET `sent` = true WHERE `id` = '$id'";
        $sql = sprintf($tpl, $this->getTableName());
        $query = $this->db()->prepare($sql);
        $query->execute();
    }

}