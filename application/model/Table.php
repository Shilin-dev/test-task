<?php
namespace application\model;

use \application\config\Database;

abstract class Model_Table
{
    public function db()
    {
        return Database::getConnection();
    }

    abstract function getTableName();

    public function insertRow($data)
    {
        $fields = $values = array();
        foreach ($data as $field => $value) {
            $fields[] = '`' . $field . '`';
            $values[] = ':' . $field;
        }
        $fields = implode(',', $fields);
        $values = implode(',', $values);
        $tableName = $this->getTableName();
        $sql = "INSERT INTO `$tableName` ($fields) VALUES($values)";
        $query = $this->db()->prepare($sql);
        foreach ($data as $field => $value) {
            $query->bindValue(':' . $field, $value);
        }
        $query->execute();
        return $this->db()->lastInsertId();
    }
}