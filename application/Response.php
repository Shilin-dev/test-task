<?php
namespace application;

class Response
{

    protected $_data = array('success' => true);

    protected $_JsonpCallback;

    public function getData()
    {
        return $this->_data;
    }

    public function success($data = null)
    {
        if (is_array($data)) {
            $this->_data = array_merge($this->_data, $data);
        }
    }

    public function error($message = null)
    {
        $this->_data = array('success' => false, 'error' => $message);
    }

    public function send()
    {
        if (!$this->_data['success']) {
            header('HTTP/1.1 400 Bad Request', true, 400);
        }
        $data = json_encode($this->_data);
        if ($this->_JsonpCallback) {
            header('Content-type: text/javascript');
            $callbackName = $this->_JsonpCallback;
            echo "$callbackName($data);";
        } else {
            echo $data;
        }
    }

    public function setJSONP($callbackName)
    {
        $this->_JsonpCallback = $callbackName;
    }
}