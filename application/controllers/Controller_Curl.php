<?php

namespace application\controllers;

require_once('../Response.php');

use application\Response;

class Controller_Curl
{

    public function __construct()
    {
        if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
            $this->checkCaptcha($_POST['g-recaptcha-response']);
        }
        return false;
    }

    public function checkCaptcha($recaptcha)
    {
        $google_url = "https://www.google.com/recaptcha/api/siteverify";
        $secret = '6LeS9SUTAAAAAI1amVA-p6NSg1hy1cqy8bHxKN9u';
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = $google_url . "?secret=" . $secret . "&response=" . $recaptcha . "&remoteip=" . $ip;
        $res = Controller_Curl::getCurlData($url);
        $res = json_decode($res, true);
        $response = new Response();
        if (!$res['success']) {
            $response->error("Ошибка каптчи");
        }
        $response->send();
    }

    static public function getCurlData($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt(
            $curl, CURLOPT_USERAGENT,
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16"
        );
        $curlData = curl_exec($curl);
        curl_close($curl);
        return $curlData;
    }

} 