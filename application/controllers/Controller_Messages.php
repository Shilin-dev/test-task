<?php
namespace application\controllers;

require_once('../model/Table.php');
require_once('../model/table/Table_Model_Messages.php');
require_once('../config/Database.php');
require_once('../Response.php');

use \application\model\table\Table_Model_Messages;
use \application\Response;

class Controller_Messages
{

    function __construct()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['data']) && !empty($_POST['data'])) {
            $this->postAction();
        }

        return false;
    }

    public function postAction()
    {
        $data = json_decode($_POST['data'], true);
        $data['date_created'] = date('Y-m-d H:i:s', time());
        $model = new Table_Model_Messages;
        $response = new Response();
        try {
            $model->insertRow($data);
        } catch (\PDOException $e) {
            $response->error($e->getMessage());
        }

        $response->send();

    }

}

$message = new Controller_Messages();
?>
