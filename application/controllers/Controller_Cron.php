<?php

namespace application\controllers;

use application\model\table\Table_Model_Messages;

class Controller_Cron
{

    public $model = null;

    public function __construct()
    {
        $this->model = new Table_Model_Messages();
    }

    public function sendMessages()
    {
        $notAvaibleDomains = array('mail.ru', 'rambler.ru', 'yandex.ru');
        $messages = $this->model->fetchSentMessages();
        foreach ($messages as $message) {
            $domain = mb_strtolower(substr(strstr($message->email, '@'), 1));
            if (array_search($domain, $notAvaibleDomains)) {
                $this->sendMessage($message, $domain);
            }
            $this->sendMessage($message);
            $this->model->changeSentStatusById($message->id);
        }

    }

    public function sendMessage($message, $notAvaibledomain = false)
    {
        $to = $message->email;
        $subject = $notAvaibledomain ? 'К сожалению, мы не можем вам помочь' : 'Всё отлично';
        $message = $notAvaibledomain
            ?
            "$message->name, добрый день! Мы не можем вам ничем помочь, т.к. Не работаем с клиентами $notAvaibledomain"
            :
            "$message->name, добрый день! Очень рады, что вы выбрали $notAvaibledomain. Ваш запрос:
            '$message->message' будет обработан в ближайшее время. Удачи!";
        $headers = "From: Test-Task <abc@example.com> \r\n Content-type: text/plain; charset=utf-8 \r\n";
        mail($to, $subject, $message, $headers);
    }
}