/**
 * Created by Jeremy on 26.07.16.
 */

$(document).ready(function () {

    $('#popup_captcha').hide();

    $('input#name, input#email, textarea#message').unbind().blur(function () {

        var id = $(this).attr('id');
        var val = $(this).val();

        switch (id) {
            case 'name':
                var rv_name = /^[А-ЯЁ][а-яё]+(-[А-ЯЁ][а-яё]+)? [А-ЯЁ][а-яё]+( [А-ЯЁ][а-яё]+)?$/;
                if (val.length > 2 && val.length <= 100 && val != '' && rv_name.test(val)) {
                    $(this).addClass('not_error').removeClass('error');
                    $(this).next('.error-box').html('<br/>');
                } else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').html('&bull; поле "ФИО" обязательно для заполнения <br> &bull; длина имени должна составлять не менее 2 символов и не более 100<br> &bull; поле должно содержать только русские буквы')
                        .css('color', 'red')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;
            case 'email':
                var rv_email = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
                if (val != '' && rv_email.test(val)) {
                    $(this).addClass('not_error').removeClass('error');
                    $(this).next('.error-box').html('<br/>');
                } else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').html('&bull; поле "Email" обязательно для заполнения<br> &bull; поле должно содержать правильный email-адрес<br> (например: example123@mail.ru)')
                        .css('color', 'red')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;
            case 'message':
                if (val != '' && val.length < 5000) {
                    $(this).addClass('not_error').removeClass('error');
                    $(this).next('.error-box').html('<br/>');
                } else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').html('&bull; поле "Сообщение" обязательно для заполнения')
                        .css('color', 'red')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;
        }
    });


//    $('form#feedback-form').submit(function (e) {
//        e.preventDefault();
//        if ($('.not_error').length == 3) {
//            $('#popup_captcha').show();
//        } else {
//            setTimeout(function () {
//                $('.input-error-box').text('<br/>');
//            }, 1000);
////            $('.input-error-box').html('&bull; Заполните все поля')
////                .css('color', 'red')
////                .animate({'paddingLeft': '10px'}, 400)
////                .animate({'paddingLeft': '5px'}, 400);
//
//        }
//    });

    $('.clear_button').click(function () {
        $('.error-box').html('<br/>');
        $('.error').removeClass('error');
    });

});

function formSubmit() {
    if ($('.not_error').length == 3) {
        $('#popup_captcha').show();
    } else {
        setTimeout(function () {
            $('.input-error-box').html('<br/>');
        }, 3000);
        $('.input-error-box').html('&bull; Заполните все поля')
            .css('color', 'red')
            .animate({'paddingLeft': '10px'}, 400)
            .animate({'paddingLeft': '5px'}, 400);
    }
}

function captchaCallback() {
    var data = {
        name: $('#name').val(),
        message: $('#message').val(),
        email: $('#email').val()
    };
    data = JSON.stringify(data);
    $.ajax({
        url: 'application/controllers/Controller_Curl.php',
        type: 'post',
        data: {
            'g-recaptcha-response': $('#g-recaptcha-response').val()
        },
        beforeSend: function () {
            $('form#feedback-form :input').attr('disabled', 'disabled');
        },

        success: function (response) {
            $('form#feedback-form :input').removeAttr('disabled');
            $.ajax({
                url: 'application/controllers/Controller_Messages.php',
                type: 'post',
                data: {
                    'data': data
                },
                success: function (data) {
                    data = JSON.parse(data);
                    $('#popup_captcha').hide();
                    $('form#feedback-form :text, textarea').val('').removeClass().next('.error-box').text('');
                    if (data.success) {
                        $('#popup-block-text').text('Запрос сохранён удачно')
                            .css('color', 'green')
                            .css('text-align', 'center')
                            .css('margin-top', '18%')
                            .css('font-size', '30px')
                            .animate({'paddingLeft': '10px'}, 400)
                            .animate({'paddingLeft': '5px'}, 400);
                    } else {
                        $('#popup-block-text').text(data.error)
                            .css('color', 'red')
                            .css('text-align', 'center')
                            .css('margin-top', '18%')
                            .css('font-size', '20px')
                            .animate({'paddingLeft': '10px'}, 400)
                            .animate({'paddingLeft': '5px'}, 400);
                    }
                    $('#popup').show();
                    setTimeout(function () {
                        $('.input-error-box').html('<br/>')
                    }, 3000);
                    grecaptcha.reset();
                }
            });
        }
    });

}
