<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Форма</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="./skins/style.css"/>
</head>
<body>
<div id="feedback-box" class="parent">
    <div class="block">
        <form id="feedback-form" method="post" action="javascript:void(0);" onsubmit="ajax()">

            <div class="input-block w1 cl">
                <label for="name">ФИО:</label><br/>
                <input type="text" name="name" id="name" value=""/>
                <div class="error-box">&nbsp;</div>
            </div>
            <div class="input-block msg ">
                <label for="message">Сообщение:</label><br/>
                <textarea name="message" id="message"></textarea>
                <div class="error-box">&nbsp;</div>
            </div>
            <div class="input-block w2 cl">
                <label for="email">email-адрес:</label><br/>
                <input type="text" name="email" id="email" value=""/>
                <div class="error-box">&nbsp;</div>
            </div>
            <input type="reset" name="clear" class="clear_button" value="Очистить">
            <input type="button" name="send" class="send" value="Отправить" onclick="formSubmit();">
            <div class="input-error-box error-box">&nbsp;</div>
        </form>
        <div id="results"></div>
    </div>
</div>
<div id="popup_captcha" class="parent">
    <div  class="block_captcha">
        <div class="g-recaptcha" data-sitekey="6LeS9SUTAAAAAAK18HFe2ojK8KLw27iWomuLTiFs" data-callback='captchaCallback'></div>
    </div>
</div>
<div id="popup" class="parent popup-parent hide">
    <div  class="popup-callback">
        <div id="popup_block">
            <div id="popup-block-text">#</div>
            <input class="popup-button" type="button" onclick="$('#popup').hide();" value="Закрыть">
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./js/main.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
</html>
